package com.example.amir.hudvy.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;

import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amir.hudvy.R;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    private static String mFileName = null;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};


    private MediaRecorder mMediaRecorder;
    private MediaPlayer mPlayer;
    private TextView tvspeed;
    private AppCompatImageView imgv;

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
            return;
        }

    }

    private void init() {
        mFileName = String.format(new Locale("en"), "%s/%s", getExternalCacheDir().getAbsolutePath(), "audiorecordtest.3gp");
        tvspeed = findViewById(R.id.textViewSpeed);

        RelativeLayout main = new RelativeLayout(this);
        main.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
        tvspeed.setGravity(Gravity.LEFT);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        Size sz = getscreensize();
        layoutParams.setMargins(sz.getWidth() / 3, sz.getHeight() / 3 - sz.getHeight() / 6, 0, 0);
        tvspeed.setLayoutParams(layoutParams);
        showSpeed();
        // new Thread(new ChangeSpeed()).start();
        //startRecording();

    }

    final int random = ThreadLocalRandom.current().nextInt(26);

    private void showSpeed() {
        new Thread(new Runnable() {
            int currentspeed = 50;
            int MaxSpeed=98;
            public void run() {
                int minspeed = currentspeed;
                int step = 1;
                while (true) {
                    currentspeed += step;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    handler.post(new Runnable() {
                        public void run() {
                            tvspeed.setText(currentspeed + "");
                        }
                    });

                    if (currentspeed > MaxSpeed)
                        step = -1;
                    if (currentspeed < minspeed)
                        step = 1;
                    if (currentspeed > MaxSpeed || currentspeed < minspeed)
                        minspeed = ThreadLocalRandom.current().nextInt(60);
                }
            }


        private int doWork ( int i){
            return i + 1;
        }

    }).

    start();
}

    public Size getscreensize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        return new Size(height, width);
    }


    private void startRecording() {
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mMediaRecorder.setOutputFile(mFileName);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mMediaRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mMediaRecorder.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopRecording();
            }
        }, 1000);
    }

    private void stopRecording() {
        mMediaRecorder.stop();
        mMediaRecorder.release();
        mMediaRecorder = null;

        startPlaying();
    }

    private void startPlaying() {
        Log.d("AAA", "start playing...");

        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION && ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            init();
        }
    }

class ChangeSpeed implements Runnable {
    private TextView tvs;

    ChangeSpeed() {
        this.tvs = findViewById(R.id.textViewSpeed);
    }

    @Override
    public void run() {
        for (int i = 0; i <= 10; i++) {
            //while (true) {
            try {
                int spd = Integer.parseInt(tvs.getText().toString());
                spd = spd > 200 ? 20 : spd + 3;
                tvs.setText(spd + "");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //}
    }

}

    public void myclick(View v) {

        String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
//        Date currentTime = Calendar.getInstance().getTime();
//        String sdt = currentTime.getTime();
        Toast.makeText(MainActivity.this, "HI \r\n" + formattedDate, Toast.LENGTH_LONG).show();
    }
}
