package com.example.amir.hudvy.core;

import android.content.Context;

public class Application extends android.app.Application {

    private static  Application instance;
    private volatile Context context;

    public static Application getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance= this;
        context = getApplicationContext();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context){
        this.context = context;
    }
}
